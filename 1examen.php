<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <h2>EJERCICIO NUMERO 1 DEL EXAMEN DE PHP</h2>
        <p>Escriba un número (0 < número <= 10) y dibujaré una tabla de una columna de ese tamaño con cajas de texto en cada celda</p>
        <h4>Ejercicio 1</h4>
        <form action="1Destinoexamen.php" method="post">
            <div>
                <label for="tabla">
                    Tamaño de la tabla: 
                </label>
                <input type="text" name="tabla" id="tabla" value="Introduce un número" required="">
            </div>
            <div>
                <input type="submit" value="Dibujar">
            </div>
            <div>
                <input type="reset" value="Restablecer">
            </div>
        </form>
        <?php
            
        ?>
    </body>
</html>